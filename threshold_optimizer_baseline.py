import warnings
import pandas as pd
import numpy as np
from sklearn import metrics
from fairlearn import metrics as fairmetrics
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from fairlearn.postprocessing import ThresholdOptimizer
from folktables import ACSDataSource, ACSEmployment
import seaborn as sns
from matplotlib import pyplot as plt

data_source = ACSDataSource(survey_year="2018", horizon="1-Year", survey="person")
acs_data = data_source.get_data(states=["AL"], download=True)
features, label, group = ACSEmployment.df_to_numpy(acs_data)
group = group == 1

base_model = make_pipeline(StandardScaler(), LogisticRegression())
tp_parity = ThresholdOptimizer(
    estimator=base_model,
    constraints="true_positive_rate_parity",
    predict_method="decision_function",
    prefit=False,
)
demographic_parity = ThresholdOptimizer(
    estimator=base_model,
    constraints="demographic_parity",
    predict_method="decision_function",
    prefit=False,
)


def eval_one_split(model, random_state):
    X_train, X_test, y_train, y_test, group_train, group_test = train_test_split(
        features, label, group, test_size=0.2, random_state=random_state
    )
    try:
        model.fit(X_train, y_train)
    except TypeError:
        model.fit(X_train, y_train, sensitive_features=group_train)
    try:
        yhat = model.predict(X_test)
    except TypeError:
        yhat = model.predict(X_test, sensitive_features=group_test)

    group_1_tpr = np.mean(yhat[(y_test == 1) & (group_test)])
    group_0_tpr = np.mean(yhat[(y_test == 1) & (~group_test)])

    # Equality of opportunity violation: in original example was 0.0871

    result =  {
        "accuracy": metrics.accuracy_score(y_test, yhat),
        "equality of opportunity": group_1_tpr - group_0_tpr,
    }

    for metric_name in "demographic_parity_difference", "equalized_odds_difference":
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            result[metric_name] = getattr(fairmetrics, metric_name)(y_test, yhat, sensitive_features=group_test)
    return result


def eval_model(model, model_name):
    print(f"\033[1m\n{model_name}\033[22m", end=" ", flush=True)
    scores = []
    for i in range(10):
        scores.append(eval_one_split(model, i))
        print(".", end="", flush=True)
    print()
    scores = pd.DataFrame(scores)
    print(scores.mean())
    scores["model_name"] = model_name
    return scores


all_models = [
    (base_model, "Fairness unaware LogisticRegression"),
    (tp_parity, "ThresholdOptimizer TP parity"),
    (demographic_parity, "ThresholdOptimizer demographic parity"),
]
all_scores = []
for item in all_models:
    all_scores.append(eval_model(*item))

all_scores = pd.concat(all_scores)
sns.set_context("talk")

fig, ax = plt.subplots(layout="compressed")
sns.boxplot(all_scores, x="equality of opportunity", y="model_name", ax=ax)
ax.set_xlabel("Equality of opportunity difference (closer to 0 is better)")
ax.axvline(0, linestyle="--", color="k")

fig, ax = plt.subplots(layout="compressed")
sns.boxplot(all_scores, x="demographic_parity_difference", y="model_name", ax=ax)
ax.set_xlabel("Demographic parity difference (closer to 0 is better)")
ax.axvline(0, linestyle="--", color="k")


plt.show()
